var baseConfig = require('../base-karma.conf.js');
var courseWareConfig = require('../courseware-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);
  courseWareConfig(config);

  config.set({
    basePath: '../'
  });

  config.plugins.push('karma-ng-html2js-preprocessor');

  config.files.push('09-directive-scope/app.js');
  config.files.push('09-directive-scope/app.spec.js');
  config.files.push('09-directive-scope/service.js');
  config.files.push('09-directive-scope/*.html');

  if (!config.preprocessors['**/*.html']) {
    config.preprocessors['**/*.html'] = [];
  }

  config.preprocessors['**/*.html'].push('ng-html2js');

  config.ngHtml2JsPreprocessor = {
    stripPrefix: '09-directive-scope/',
    moduleName: 'bacon.templates'
  }
};

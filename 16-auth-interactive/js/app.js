'use strict';

function authInterceptor($injector, $q) {

  return {
    responseError: function(response) {
      return $injector.invoke(function(authModal, authService, $http) {

        function isRecoverable(response) {
          return response.status === 401 && !authService.isLoginUrl(response.config.url);
        }

        if (isRecoverable(response)) {

          // TODO 2.1 - zobrazte modální okno
          // TODO 3.1 - opakovaně odešlete požadavek
         
          
        }

        return $q.reject(response);
      });
    }
  };
}

function tokenInterceptor($injector) {
  var AUTH_HEADER = 'X-Auth-Token';

  return {
    request: function(config) {

      var authService = $injector.get('authService');
      if (authService.isAuthenticated()) {
        config.headers = config.headers || {};
        config.headers[AUTH_HEADER] = authService.getToken();
      }

      return config;
    }
  };
}

function configInterceptors($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
  $httpProvider.interceptors.push('tokenInterceptor');
}

function configRouter($routeProvider) {

  $routeProvider
    .when('/', {
      templateUrl: 'orderList.html',
      controller: 'OrderListController',
      controllerAs: 'list',
      resolve: {
        ordersData: function(Order) {
          return Order.query().$promise;
        }
      }
    })

    .when('/detail/:id', {
      templateUrl: 'orderDetail.html',
      controller: 'OrderDetailController',
      controllerAs: 'detail',
      resolve: {
        orderData: function(Order, $route) {
          var id = $route.current.params.id;
          return Order.get({'id': id}).$promise;
        }
      }
    })

    .when('/create', {
      templateUrl: 'orderCreate.html',
      controller: 'OrderCreateController',
      controllerAs: 'create',
      resolve: {
        isAuthorized: function(authService, authModal) {

          // TODO 5.1 - zobrazte modální okno pro přihlášení
          return authService.isAuthenticated();
        }
      }
    })

    .otherwise('/');
}

angular.module('authApp', ['ngRoute', 'ngMessages', 'ngResource', 'ui.bootstrap'])
  .constant('REST_URI', '//angular-cz-security-api.herokuapp.com')

  .factory("authInterceptor", authInterceptor)
  .factory("tokenInterceptor", tokenInterceptor)

  .run(function($rootScope, $location) {
    $rootScope.$on('login:loggedOut', function() {
      $location.path("/")
    });
  })

  .run(function($rootScope, $location) {
    // TODO 5.2 - reagujte na zprávu $routeChangeError
    $rootScope.$on('', function() {
      $location.path('/');
    });
  })

  .config(configInterceptors)
  .config(configRouter);

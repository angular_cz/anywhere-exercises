# Cvičení - www.angular.cz #

## Co si nainstalovat
- [Java Development Kit (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/index.html )
- [git](http://git-scm.com/downloads)
- doporučenou verzi [nodeJS](https://nodejs.org/en/) - 6.x

Pokud instalujete Git na Windows, zvolte možnost pro instalaci i do Windows command prompt - viz [obrázek](http://wiki.team-mediaportal.com/@api/deki/files/3808/=Git_Setup_-_Run_from_Windows_Command_Prompt.PNG) 

## NPM závislosti
Nodejs se nainstaloval spolu s balíčkovacím nástrojem npm.

## Ověření správnosti instalace

Stáhněte tento balíček kamkoli k sobě. 

```
git clone https://bitbucket.org/angular_cz/anywhere-exercises
```

Spusťte ve staženém adresáři příkaz pro instalaci závislostí. 

Pokud používáte windows, ujistěte se, že jej spouštíte v shellu, kde máte k dispozici jak git tak node.
V případě, že si nejste jistí, můžete použít například "Git Bash" nainstalovaný spolu s gitem.


```
 npm install
```

Důležité je, aby instalace chybou neskončila. Během spuštění se mohou objevovat warningy a modul node-gyp pravděpodobně vyhlásí několik chyb, i přesto by měla instalace doběhnout v pořádku. Ignorujte výzvu k instalaci pythonu a visual studia.

Poté spusťte cvičení pomocí následujícího příkazu. Příkaz by neměl skončit chybou, v průběhu spouštění se objeví červené výsledky inicializace testů, to je v pořádku.

```
 npm start
```

Nyní, když otevřete prohlížeč na adrese [http://localhost:8000/](http://localhost:8000/), uvidíte seznam cvičení.

### Test protractoru ###

Protractor je nástroj pro end 2 end testování, pokud by toto byl jediný krok, který Vám nebude fungovat. Nemusíte se tím trápit nejedná se o důležitou věc.

Nyní nechte spuštěný příkaz "npm start" a v novém terminálu ve složce projektu spusťte příkaz.

```
 npm run test-protractor
```

Příkaz by opět neměl hlásit chybu . Měli byste vidět výstup testu:

```
Starting selenium standalone server...
Selenium standalone server started at http://192.168.1.17:56131/wd/hub
.

Finished in 1.515 seconds
1 test, 1 assertion, 0 failures
```

Pokud vše proběhlo podle postupu, máte správně připravené prostředí. 
V opačném případě se podívejte do sekce možné problémy a pokud tam nenaleznete řešení, kontaktujte nás.

### Volba editoru ###

Otevřete si složku cvičení ve vašem oblíbeném editoru, tak, abyste viděli všechny příklady jako strom.

Velmi Vám doporučujeme použít editor, který umí syntaxi javascriptu a automatické formátování. Pokud to Váš editor neumí, můžete sáhnout po jednom z těchto:

 - Webstorm - [https://www.jetbrains.com/webstorm/download/] (https://www.jetbrains.com/webstorm/download/) - 30 dní trial
 - Netbeans - [https://netbeans.org/downloads/] (https://netbeans.org/downloads/) - zdarma
  
Prima, první půlku máme za sebou. 

#### Poznámka k editorům ####
Některé editory (například Webstorm) ukládají soubory tak, že se jej nejprve zapíšou do dočasného úložiště a poté překopírují na cílové místo. Toto způsobuje problémy při sledování změn souborů, pro účely kurzu tuto vlastnost vypněte (Webstorm ji označuje jako "safe write")

## Použití při cvičeních: ##

Všechny potřebné nástroje a závislosti jste nainstalovali už v předchozích krocích pomocí příkazu "npm install".

### Při dalším spouštění už si tak vystačíte s příkazem: ###
```
npm start
```
který spustí lokální server na adrese [http://localhost:8000/](http://localhost:8000/)

### Možné problémy ###

####unable to connect to github.com####
Pokud vidíte tuto chybovou zprávu po spuštění *bower install*
 - máte buď blokováno připojení ke githubu  - to můžete ověřit otevřením github.com v prohlížeči
 - nebo máte blokován protokol git - spusťte příkaz, který "přesměruje" protokol git po https

```
git config url."https://github.com/".insteadOf git@github.com:
git config url."https://".insteadOf git://
```

Nyní už by měl příkaz *bower install* fungovat

Pokud problémy přetrvávají, a jste uživatelem systému Windows, může zde být následující problém.

 - Nastavení výše se zapíšou do .gitconfig do domovské složky. Na Windows s profilem na vzdáleném disku však může každý ze shellů hledat home jinde.
 - Pokud je toto Váš případ, zkopírujte soubor do obou umístění, na sdílenou domovskou složku a c:\users\[name]\

Chcete-li používat konfiguraci i pro další projekty, můžete ji nastavit globálně (přidat atribut --global)

```
git config --global url."https://github.com/".insteadOf git@github.com:
git config --global url."https://".insteadOf git://
```

#### bower install ####
Pokud Vám instalace selže s **chybou Failed at the courseware@xxxx postinstall script : bower install** - nepodařilo se spustit stažení závislostí.

1. nainstalujte bower globálně pomocí : **npm install -g bower**
2. smažte složku **node_modules**
3. spusťte znovu:  **npm install**


####spawn child ENOENT error####
Pokud při spuštění *npm run update-webdriver*, nebo *npm run protractor* vidíte tuto chybu, pravděpodobně nemáte nainstalovanou JAVU, která je nutná pro jeho spuštění. Pozor na to, že na Windows musíte po instalaci Javy spustit příkazovou řádku znovu, jinak se bude chyba opakovat.

####Error: Angular could not be found on the page http://localhost:8888/app/index.html : retries looking for angular exceeded####
pokud při  *npm run protractor* vidíte tuto chybu, ověřte, že na adrese http://localhost:8888/app/index.html je vidět testovací stránka. Pokud není, pravděpodobně jste vypnuli konzoli se spuštěným "npm start" po první části testu.

##### Adresa pro ruční stažení chrome driveru #####
Chromedriver nejvyšší verze: [http://chromedriver.storage.googleapis.com/index.html](http://chromedriver.storage.googleapis.com/index.html) aktuálně 2.25

Selenium server : [http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.0.jar](http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.0.jar)
'use strict';

function UserController($scope, $rootScope) {
  this.user = {
    name: 'Petr',
    surname: 'Novák',
    yearOfBirth: 1982
  };

  // TODO 4.1 - napište watch sledující změnu yearOfBirth
  // TODO 4.2 - přepište expression na funkci
  // TODO 5.1 - vyšlete zprávu yearOfBirth:changed
  // TODO 6.2 - odesílejte zprávu na $rootScope

  this.getAge = function() {
    // TODO 2 - odkomentujte
    //console.log('getAge');
    var now = new Date();
    return now.getFullYear() - this.user.yearOfBirth;
  };
}

function CountController($scope) {
  this.count = 0;

  // TODO 5.2 - poslouchejte zprávu yearOfBirth:changed
}

angular.module('moduleExample', [])
  .controller('UserController', UserController)
  .controller('CountController', CountController);

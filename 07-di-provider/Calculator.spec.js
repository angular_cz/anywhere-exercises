describe('calculator', function() {

  beforeEach(function() {
    module('diApp');

    // TODO 5 - konfigurace provideru pro účely testu
  });

  it('should return 90 when format is A5 and number of pages is 0', inject(function(calculator) {

      var product = {
        pageSize: 'A5',
        numberOfPages: 0
      };

      expect(calculator.getPrice(product)).toEqual(90);
    })
  );

  it('should return 120 when format is A5 and number of pages is 50', inject(function(calculator) {

    var product = {
      pageSize: 'A5',
      numberOfPages: 50
    };

    expect(calculator.getPrice(product)).toEqual(120);
  }));

});

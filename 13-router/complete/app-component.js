'use strict';

function configRouter($routeProvider) {

  $routeProvider
    .when('/orders', {
      template : '<order-list></order-list>'

    })
    .when('/detail/:id', {
      template: '<order-detail order="$resolve.orderData"></order-detail>',
      resolve: {
        orderData: function($http, $route, REST_URI) {
          return $http.get(REST_URI + '/orders/' + $route.current.params.id)
            .then(function(response) {
              return response.data;
            });
        }
      }
    })
    .when('/create', {
      template : '<order-create></order-create>'
    })
    .otherwise('/orders');
}

function OrderListController($http, REST_URI) {
  this.orders = [];

  this.statuses = {
    NEW: 'Nová',
    MADE: 'Vyrobená',
    CANCELLED: 'Zrušená',
    PAID: 'Zaplacená',
    SENT: 'Odeslaná'
  };

  this.onOrdersLoad = function(orders) {
    this.orders = orders;
  };

  $http.get(REST_URI + '/orders')
    .then(function(response) {
      return response.data;
    })
    .then(this.onOrdersLoad.bind(this));
}


function OrderCreateController($http, $location, REST_URI) {
  this.save = function(order) {
    $http.post(REST_URI + '/orders', order)
      .then(function() {
        $location.path('/orders');
      });
  }
}
angular.module('orderAdministration', ['ngRoute', 'ngMessages'])
  .constant('REST_URI', '//angular-cz-orders-api.herokuapp.com')
  .config(configRouter)
  .component('orderList', {
    templateUrl: 'orderList.html',
    controller: OrderListController,
    controllerAs: 'list'
  })
  .component('orderDetail', {
    bindings: {
      order : "<"
    },
    templateUrl: 'orderDetail.html',
    controllerAs: 'detail'
  })
  .component('orderCreate', {
    templateUrl: 'orderCreate.html',
    controller: OrderCreateController,
    controllerAs: 'create'
  });

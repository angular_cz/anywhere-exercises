'use strict';

function configRouter($routeProvider) {

  $routeProvider
    .when('/orders', {
      templateUrl: 'orderList.html',
      controller: 'OrderListController',
      controllerAs: 'list'
    })
    .when('/detail/:id', {
      templateUrl: 'orderDetail.html',
      controller: 'OrderDetailController',
      controllerAs: 'detail',
      resolve: {
        orderData: function($http, $route, REST_URI) {
          return $http.get(REST_URI + '/orders/' + $route.current.params.id)
            .then(function(response) {
              return response.data;
            });
        }
      }
    })
    .when('/create', {
      templateUrl: 'orderCreate.html',
      controller: 'OrderCreateController',
      controllerAs: 'create'
    })
    .otherwise('/orders');
}

function OrderListController($http, REST_URI) {
  this.orders = [];

  this.statuses = {
    NEW: 'Nová',
    MADE: 'Vyrobená',
    CANCELLED: 'Zrušená',
    PAID: 'Zaplacená',
    SENT: 'Odeslaná'
  };

  this.onOrdersLoad = function(orders) {
    this.orders = orders;
  };

  $http.get(REST_URI + '/orders')
    .then(function(response) {
      return response.data;
    })
    .then(this.onOrdersLoad.bind(this));
}

function OrderDetailController(orderData) {
  this.order = orderData;
}

function OrderCreateController($http, $location, REST_URI) {
  this.save = function(order) {
    $http.post(REST_URI + '/orders', order)
      .then(function() {
        $location.path('/orders');
      });
  }
}
angular.module('orderAdministration', ['ngRoute', 'ngMessages'])
  .constant('REST_URI', '//angular-cz-orders-api.herokuapp.com')
  .config(configRouter)
  .controller('OrderListController', OrderListController)
  .controller('OrderDetailController', OrderDetailController)
  .controller('OrderCreateController', OrderCreateController);

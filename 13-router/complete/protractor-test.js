'use strict';
var DetailPO = function() {
  this.getTitle = function() {
    return element(by.css('h1')).getText();
  };
};

var CreatePO = function() {
  this.goTo = function() {
    browser.get('index.html#/create');
  };

  this.getTitle = function() {
    return element(by.css('h1')).getText();
  };

  this.clickSaveButton = function() {
    var submitButton = element(by.buttonText('Uložit'));
    submitButton.click();
  };

  this.setUserName = function(userName) {
    var nameInput = element(by.model('create.order.name'));
    nameInput.clear().sendKeys(userName);
  };

  this.setEmail = function(email) {
    var emailInput = element(by.model('create.order.email'));
    emailInput.clear().sendKeys(email);
  }
};

var OrdersPO = function() {
  this.repeater = element.all(by.repeater('order in list.orders'));

  this.goTo = function() {
    browser.get('index.html#/orders');
  };

  this.clickCreateLink = function() {
    var createLink = element(by.linkText('Vytvořit'));
    createLink.click();
  };

  this.getRowForName = function(userName) {
    var rowWithUser = this.repeater.filter(function(elem, index) {
      return elem.getText().then(function(text) {
        return text.match(userName);
      });
    }).first();

    return new RowPO(rowWithUser);
  };

  this.getRow = function(index) {
    return new RowPO(this.repeater.get(index));
  }
};

var RowPO = function(row) {

  this.getName = function() {
    var name = row.element(by.binding('order.name'));
    return name.getText();
  };

  this.clickDetailLink = function() {
    var linkToDetail = row.element(by.linkText('Detail'));
    linkToDetail.click();
  }
};

describe('13-router example', function() {

  beforeAll(function() {
    this.createPO = new CreatePO();
    this.ordersPO = new OrdersPO();
    this.detailPO = new DetailPO();
  });

  it('should redirect to list when url is invalid', function() {
    browser.get('index.html#/invalid-url');
    expect(browser.getLocationAbsUrl()).toBe('/orders');
  });

  it('should have route /create with creation form', function() {

    this.createPO.goTo();

    expect(this.createPO.getTitle())
      .toMatch('Nová objednávka pro uživatele');
  });

  describe('creation form', function() {
    var userId;
    var userName;

    beforeAll(function() {
      userId = Math.ceil(Math.random() * 1000);
      userName = 'Protractor' + userId;
      var email = userId + '@protractor';

      this.createPO.goTo();

      this.createPO.setUserName(userName);
      this.createPO.setEmail(email);
      this.createPO.clickSaveButton();
    });

    it('should have redirect to orders after creation', function() {
      expect(browser.getLocationAbsUrl()).toBe('/orders');
    });

    it('should have user in list after creation', function() {
      this.ordersPO.goTo();

      var row = this.ordersPO.getRowForName(userName);
      expect(row.getName()).toBe(userName);
    });
  });

  describe('list', function() {
    it('should link to creation form from list', function() {
      this.ordersPO.goTo();
      this.ordersPO.clickCreateLink();

      expect(browser.getLocationAbsUrl()).toBe('/create');
    });
  });

  it('should redirect to detail on click', function() {
    this.ordersPO.goTo();
    var row = this.ordersPO.getRow(0);
    var name = row.getName();

    row.clickDetailLink();

    expect(browser.getLocationAbsUrl()).toMatch('/detail');

    expect(this.detailPO.getTitle()).toMatch(name);
  });
});

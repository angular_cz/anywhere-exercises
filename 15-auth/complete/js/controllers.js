'use strict';

function OrderListController(Order, authService) {
  this.authService = authService;

  this.orders = Order.query();

  this.statuses = {
    NEW: 'Nová',
    CANCELLED: 'Zrušená',
    PAID: 'Zaplacená',
    SENT: 'Odeslaná'
  };

  this.update = function(order) {
    order.$save();
  };

  this.remove = function(order) {
    var removeFromArray_ = function(order) {
      var index = this.orders.indexOf(order);
      this.orders.splice(index, 1);
    }.bind(this);

    order.$remove()
      .then(removeFromArray_);
  };
}

function OrderDetailController(orderData) {
  this.order = orderData;
}

function OrderCreateController(Order, $location) {
  this.order = new Order();

  this.save = function() {
    this.order.$save(function() {
      $location.path("/orders");
    });
  };
}

function HomeCtrl($location, authService) {

  this.authenticate_ = function(user, pass) {
    var redirectToOrders = function() {
      $location.path('/orders');
    };

    var showBadLogin = function() {
      this.isBadLogin = true;
    }.bind(this);

    authService.login(user, pass)
      .then(redirectToOrders)
      .catch(showBadLogin);
  };

  this.loginUser = function() {
    this.authenticate_("user", "password");
  };

  this.loginAdmin = function() {
    this.authenticate_("operator", "password");
  };

  this.loginFail = function() {
    this.authenticate_("badUser", "badPassword");
  };
}

angular.module('authApp')
  .controller('OrderListController', OrderListController)
  .controller('OrderDetailController', OrderDetailController)
  .controller('OrderCreateController', OrderCreateController)
  .controller('HomeCtrl', HomeCtrl);
